<?php

namespace Octa\Framework\Modules\Console;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Command Name for the Octa Console.
     *
     * @var array
     */
    protected $commandName = [
        'octa.module.install',
        'octa.module.make',
        'octa.controller.make',
    ];

    /**
     * Command Identifier for the Octa Console.
     *
     * @var array
     */
    protected $commands;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
    }

    protected function registerCommands()
    {
        foreach ($this->commandName as $commandName) {
            $methodName = 'register'.implode(array_map('ucfirst', explode('.', $commandName)));
            $this->$methodName();

            $this->commands[] = 'command.'.$commandName;
        }

        $this->commands($this->commands);
    }

    /**
     * Register the Octa Module Make .
     *
     * @return void
     */
    protected function registerOctaModuleMake()
    {
        $this->app->singleton('command.octa.module.make', function ($app) {
            return new ModuleMakeCommand($app['files']);
        });
    }

    /**
     * Register the Octa Module Install .
     *
     * @return void
     */
    protected function registerOctaModuleInstall()
    {
        $this->app->singleton('command.octa.module.install', function ($app) {
            return new ModuleInstallCommand($app['migrator']);
        });
    }

    /**
     * Register Octa Module Controller Make Command.
     *
     * @return void
     */
    protected function registerOctaControllerMake()
    {
        $this->app->singleton('command.octa.controller.make', function ($app) {
            return new ControllerMakeCommand($app['files']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return $this->commands;
    }
}
