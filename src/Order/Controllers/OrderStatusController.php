<?php

namespace Octa\Framework\Order\Controllers;

use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\OrderStatus;
use Octa\Framework\Order\Requests\OrderStatusRequest;
use Octa\Framework\Database\Contracts\OrderStatusModelInterface;

class OrderStatusController
{
    /**
     * OrderStatus Repository for the Install Command.
     * @var \Octa\Framework\Database\Repository\OrderStatusRepository
     */
    protected $orderStatusRepository;

    /**
     * Construct for the Octa install command.
     * @param \Octa\Framework\Database\Contracts\OrderModelInterface $orderStatusRepository
     */
    public function __construct(
        OrderStatusModelInterface $orderStatusRepository
    ) {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * Show Dashboard of an Octa Admin.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderStatus = $this->orderStatusRepository->all();

        return view('octa::order.order-status.index')
            ->with(compact('orderStatus'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tabs = Tab::get('order.order-status');

        return view('octa::order.order-status.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\Order\Requests\OrderStatusRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStatusRequest $request)
    {
        $this->orderStatusRepository->create($request->all());

        return redirect()->route('admin.order-status.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::order.order-status.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\OrderStatus $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderStatus $orderStatus)
    {
        $tabs = Tab::get('order.order-status');

        return view('octa::order.order-status.edit')
            ->with(compact('orderStatus', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\Order\Requests\OrderStatusRequest $request
     * @param \Octa\Framework\Database\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function update(OrderStatusRequest $request, OrderStatus $orderStatus)
    {
        $orderStatus->update($request->all());

        return redirect()->route('admin.order-status.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa::order.order-status.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderStatus $orderStatus)
    {
        $orderStatus->delete();

        return [
            'success' => true,
            'message' => __(
                'octa::system.notification.delete',
                ['attribute' => __('octa::order.order-status.title')]
            ),
        ];
    }
}
