<?php

namespace Octa\Framework\Order\Observers;

use Octa\Framework\Database\Contracts\ConfigurationModelInterface;
use Octa\Framework\Database\Models\Order;
use Octa\Framework\Widget\TotalOrder;

class OrderObserver
{
    /**
     * Handle the Order "created" event
     * @param \Octa\Framework\Database\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }
}
