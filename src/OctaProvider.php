<?php

namespace Octa\Framework;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Octa\Framework\Support\Console\AdminMakeCommand;
use Octa\Framework\Support\Console\InstallCommand;
use Octa\Framework\Support\Middlewares\AdminAuth;
use Octa\Framework\Support\Middlewares\OctaCore;
use Octa\Framework\Support\Middlewares\Permission;
use Octa\Framework\Support\Middlewares\RedirectIfAdminAuth;
use Octa\Framework\System\ViewComposers\LayoutComposer;

class OctaProvider extends ServiceProvider
{
    protected $providers = [
        \Octa\Framework\Support\Providers\BreadcrumbProvider::class,
        \Octa\Framework\Support\Providers\CartProvider::class,
        \Octa\Framework\Support\Providers\EventServiceProvider::class,
        \Octa\Framework\Support\Providers\MenuProvider::class,
        \Octa\Framework\Support\Providers\ModelProvider::class,
        \Octa\Framework\Support\Providers\ModuleProvider::class,
        \Octa\Framework\Support\Providers\PaymentProvider::class,
        \Octa\Framework\Support\Providers\PermissionProvider::class,
        \Octa\Framework\Support\Providers\ShippingProvider::class,
        \Octa\Framework\Support\Providers\TabProvider::class,
        \Octa\Framework\Support\Providers\WidgetProvider::class,
    ];

    public function register()
    {
        $this->registerProviders();
        $this->registerConfigData();
        $this->registerRoutePath();
        $this->registerMiddleware();
        $this->registerViewComposerData();
        $this->registerConsoleCommands();
        $this->registerMigrationPath();
        $this->registerViewPath();
    }

    public function boot()
    {
        $this->registerTranslationPath();
        $this->setupPublishFiles();
    }

    public function registerRoutePath()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }

    public function registerConsoleCommands()
    {
        $this->commands([InstallCommand::class]);
        $this->commands([AdminMakeCommand::class]);
    }

    public function registerMigrationPath()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    public function registerViewPath()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'octa');
    }

    public function registerTranslationPath()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'octa');
    }

    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            App::register($provider);
        }
    }

    protected function registerMiddleware()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('admin.auth', AdminAuth::class);
        $router->aliasMiddleware('permission', Permission::class);
        $router->aliasMiddleware('admin.guest', RedirectIfAdminAuth::class);
        $router->aliasMiddleware('octa', OctaCore::class);
    }

    public function registerConfigData()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/octa.php', 'octa');

        $octaConfigData = include __DIR__.'/../config/octa.php';
        $fileSystemConfig = $this->app['config']->get('filesystems', []);
        $authConfig = $this->app['config']->get('auth', []);

        $this->app['config']->set(
            'filesystems',
            array_merge_recursive(
                $octaConfigData['filesystems'],
                $fileSystemConfig
            )
        );

        $this->app['config']->set('auth', array_merge_recursive($octaConfigData['auth'], $authConfig));
    }

    public function registerViewComposerData()
    {
        View::composer('octa::layouts.app', LayoutComposer::class);
    }

    public function setupPublishFiles()
    {
        $this->publishes([
            __DIR__.'/../config/octa.php' => config_path('octa.php'),
        ], 'octa-config');

        $this->publishes([
            __DIR__.'/../assets/octa-admin' => public_path('octa-admin'),
        ], 'octa-public');
    }
}
