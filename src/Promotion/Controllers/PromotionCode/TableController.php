<?php
namespace Octa\Framework\Promotion\Controllers\PromotionCode;

use Octa\Framework\Promotion\ViewModels\PromotionTableViewModel;

class TableController
{
    /**
     * Show available promotion list of an Octa Admin.
     * @return \Illuminate\View\View
     */
    public function __invoke()
    {
        return view('octa::promotion.promotion-code.index', new PromotionTableViewModel);
    }
}
