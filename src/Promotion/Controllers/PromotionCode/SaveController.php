<?php
namespace Octa\Framework\Promotion\Controllers\PromotionCode;

use Octa\Framework\Database\Models\PromotionCode;
use Octa\Framework\Promotion\Requests\PromotionCodeRequest;

class SaveController
{
    /**
     * Save the specified resource from storage.
     * @param \Octa\Framework\Promotion\Requests\PromotionCodeRequest  $request
     * @param \Octa\Framework\Database\Models\PromotionCode $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCodeRequest $request, PromotionCode $promotionCode)
    {
        $promotionCode->fill($request->all())->save();

        return redirect()->route('admin.promotion.code.table');
    }
}
