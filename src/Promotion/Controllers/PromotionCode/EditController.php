<?php
namespace Octa\Framework\Promotion\Controllers\PromotionCode;

use Octa\Framework\Database\Models\PromotionCode;
use Octa\Framework\Promotion\ViewModels\Promotion\EditViewModel;

class EditController
{
    /**
     * Create/Edit the specified resource from storage.
     * @param \Octa\Framework\Database\Models\PromotionCode  $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCode $promotionCode)
    {
        return view(
            'octa::promotion.promotion-code.edit',
            new EditViewModel($promotionCode)
        );
    }
}
