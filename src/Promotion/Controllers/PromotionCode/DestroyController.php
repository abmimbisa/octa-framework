<?php
namespace Octa\Framework\Promotion\Controllers\PromotionCode;

use Octa\Framework\Database\Models\PromotionCode;
use Octa\Framework\Promotion\ViewModels\PromotionTableViewModel;

class DestroyController
{

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\PromotionCode  $promotionCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromotionCode $promotionCode)
    {
        $promotionCode->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'octa::system.notification.delete',
                ['attribute' => __('octa::promotion.promotion-code.title')]
            ),
        ]);
    }
}
