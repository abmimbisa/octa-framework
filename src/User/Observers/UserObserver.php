<?php

namespace Octa\Framework\User\Observers;

use Octa\Framework\Database\Contracts\ConfigurationModelInterface;
use Octa\Framework\Database\Contracts\UserGroupModelInterface;
use Octa\Framework\Widget\TotalCustomer;

class UserObserver
{
    /**
     * UserGroup Repository for controller.
     * @var \Octa\Framework\Database\Repository\UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * Construct for the Octa user group controller.
     * @param \Octa\Framework\Database\Repository\UserGroupRepository $userGroupRepository
     */
    public function __construct(
        UserGroupModelInterface $userGroupRepository
    ) {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Handle the User "created" event.
     *
     * @param mixed $user
     * @return void
     */
    public function created($user)
    {
        $userGroup = $this->userGroupRepository->getIsDefault();
        $user->user_group_id = $userGroup->id;
        $user->save();
    }
}
