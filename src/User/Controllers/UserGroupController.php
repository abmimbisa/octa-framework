<?php

namespace Octa\Framework\User\Controllers;

use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\UserGroup;
use Octa\Framework\User\Requests\UserGroupRequest;
use Octa\Framework\Database\Contracts\UserGroupModelInterface;

class UserGroupController
{
    /**
     * UserGroup Repository for controller.
     * @var \Octa\Framework\Database\Repository\UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * Construct for the Octa user group controller.
     * @param \Octa\Framework\Database\Contracts\UserGroupModelInterface $userGroupRepository
     */
    public function __construct(
        UserGroupModelInterface $userGroupRepository
    ) {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Show Dashboard of an Octa Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $userGroups = $this->userGroupRepository->all();

        return view('octa::user.user-group.index')
            ->with(compact('userGroups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('user.user-group');

        return view('octa::user.user-group.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\Cms\Requests\UserGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserGroupRequest $request)
    {
        $this->userGroupRepository->create($request->all());

        return redirect()->route('admin.user-group.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::user.user-group.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\UserGroup $userGroup
     * @return \Illuminate\View\View
     */
    public function edit(UserGroup $userGroup)
    {
        $tabs = Tab::get('user.user-group');

        return view('octa::user.user-group.edit')
            ->with(compact('userGroup', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\Cms\Requests\UserGroupRequest $request
     * @param \Octa\Framework\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        if ($request->get('is_default')) {
            $group = $this->userGroupRepository->getIsDefault();
            $group->update(['is_default' => 0]);
        }

        $userGroup->update($request->all());

        return redirect()->route('admin.user-group.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa::user.user-group.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'octa::system.notification.delete',
                ['attribute' => __('octa::user.user-group.title')]
            ),
        ]);
    }
}
