<?php

namespace Octa\Framework\User\Controllers;

use Illuminate\Routing\Controller;
use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\AdminUser;
use Octa\Framework\User\Requests\AdminUserRequest;
use Octa\Framework\User\Requests\AdminUserImageRequest;
use Octa\Framework\Database\Contracts\RoleModelInterface;
use Octa\Framework\Database\Contracts\AdminUserModelInterface;

class AdminUserController extends Controller
{
    /**
     * AdminUser Repository.
     * @var \Octa\Framework\Database\Repository\AdminUserRepository
     */
    protected $adminUserRepository;

    /**
     * Role Repository.
     * @var \Octa\Framework\Database\Repository\RoleRepository
     */
    protected $roleRepository;

    /**
     * Construct for the Octa User Controller.
     * @param \Octa\Framework\Database\Contracts\AdminUserModelInterface $adminUserRepository
     * @param \Octa\Framework\Database\Contracts\RoleModelInterface $roleRepository
     */
    public function __construct(
        AdminUserModelInterface $adminUserRepository,
        RoleModelInterface $roleRepository
    ) {
        $this->adminUserRepository = $adminUserRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $adminUsers = $this->adminUserRepository->all();

        return view('octa::user.admin-user.index')
            ->with(compact('adminUsers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('user.admin-user');
        $roleOptions = $this->roleRepository->options();

        return view('octa::user.admin-user.create')
            ->with(compact('roleOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\System\Requests\AdminUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);

        $this->adminUserRepository->create($request->all());

        return redirect()->route('admin.admin-user.index')
            ->with('successNotification', __('octa::user.notification.store', ['attribute' => 'AdminUser']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\AdminUser $adminUser
     * @return \Illuminate\View\View
     */
    public function edit(AdminUser $adminUser)
    {
        $tabs = Tab::get('user.admin-user');
        $roleOptions = $this->roleRepository->options();

        return view('octa::user.admin-user.edit')
            ->with(compact('adminUser', 'roleOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\System\Requests\AdminUserRequest $request
     * @param \Octa\Framework\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserRequest $request, AdminUser $adminUser)
    {
        $adminUser->update($request->all());

        return redirect()->route('admin.admin-user.index')
            ->with('successNotification', __('octa::user.notification.updated', ['attribute' => 'AdminUser']));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AdminUser $adminUser)
    {
        $adminUser->delete();

        return response()->json([
            'success' => true,
            'message' => __('octa::user.notification.delete', ['attribute' => 'AdminUser']),
        ]);
    }

    /**
     * upload user image to file system.
     * @param \Octa\Framework\System\Requests\AdminUserImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AdminUserImageRequest $request)
    {
        $image = $request->file('image_file');
        $path = $image->store('uploads/users', 'public');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('octa::user.notification.upload', ['attribute' => 'Admin User Image']),
        ]);
    }
}
