<?php

namespace Octa\Framework\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Octa\Framework\Database\Models\UserGroup;
use Octa\Framework\Database\Contracts\UserGroupModelInterface;

class UserGroupRepository implements UserGroupModelInterface
{
    /**
     * Create UserGroup Resource into a database.
     * @param array $data
     * @return \Octa\Framework\Database\Models\UserGroup $userGroups
     */
    public function create(array $data): UserGroup
    {
        return UserGroup::create($data);
    }

    /**
     * get all user groups for.
     * @return \Illuminate\Database\Eloquent\Collection $userGroups
     */
    public function all() : Collection
    {
        return UserGroup::all();
    }

    /**
     * get default user group instance.
     * @return \Octa\Framework\Database\Models\UserGroup $userGroup
     */
    public function getIsDefault() : UserGroup
    {
        return UserGroup::whereIsDefault(true)->first();
    }
}
