<?php

namespace Octa\Framework\Database\Repository;

use Illuminate\Database\Eloquent\Collection;
use Octa\Framework\Database\Models\TaxGroup;
use Octa\Framework\Database\Contracts\TaxGroupModelInterface;

class TaxGroupRepository implements TaxGroupModelInterface
{
    /**
     * Create TaxGroup Resource into a database.
     * @param array $data
     * @return \Octa\Framework\Database\Models\TaxGroup $taxGroups
     */
    public function create(array $data): TaxGroup
    {
        return TaxGroup::create($data);
    }

    /**
     * get all user groups for.
     * @return \Illuminate\Database\Eloquent\Collection $taxGroups
     */
    public function all() : Collection
    {
        return TaxGroup::all();
    }
}
