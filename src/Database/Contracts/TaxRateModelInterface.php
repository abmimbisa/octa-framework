<?php

namespace Octa\Framework\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Octa\Framework\Database\Models\TaxRate;

interface TaxRateModelInterface
{
    /**
     * Create TaxRate Resource into a database.
     * @param array $data
     * @return \Octa\Framework\Database\Models\TaxRate $taxGroup
     */
    public function create(array $data) : TaxRate;

    /**
     * find roles for the users.
     * @return \Illuminate\Database\Eloquent\Collection $taxGroups
     */
    public function all() : Collection;
}
