<?php

namespace Octa\Framework\System\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\Currency;
use Octa\Framework\System\Requests\CurrencyRequest;
use Octa\Framework\Database\Contracts\CountryModelInterface;
use Octa\Framework\Database\Contracts\CurrencyModelInterface;

class CurrencyController extends Controller
{
    /**
     * Currency Repository.
     * @var \Octa\Framework\Database\Repository\CurrencyRepository
     */
    protected $currencyRepository;

    /**
     * Currency Repository.
     * @var \Octa\Framework\Database\Repository\CountryRepository
     */
    protected $countryRepository;

    /**
     * Construct for the Octa currency controller.
     * @param \Octa\Framework\Database\Contracts\CurrencyModelInterface $currencyRepository
     * @param \Octa\Framework\Database\Contracts\CountryModelInterface $countryRepository
     */
    public function __construct(
        CurrencyModelInterface $currencyRepository,
        CountryModelInterface $countryRepository
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $currencies = $this->currencyRepository->all();

        return view('octa::system.currency.index')
            ->with(compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('system.currency');
        $currencyCodeOptions = $this->countryRepository->currencyCodeOptions();
        $currencySymbolOptions = $this->countryRepository->currencySymbolOptions();

        return view('octa::system.currency.create')
            ->with(compact('currencySymbolOptions', 'currencyCodeOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\System\Requests\CurrencyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CurrencyRequest $request)
    {
        $this->currencyRepository->create($request->all());

        return redirect()->route('admin.currency.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::system.currency.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\Currency $currency
     * @return \Illuminate\View\View
     */
    public function edit(Currency $currency)
    {
        $tabs = Tab::get('system.currency');
        $currencyCodeOptions = $this->countryRepository->currencyCodeOptions();
        $currencySymbolOptions = $this->countryRepository->currencySymbolOptions();

        return view('octa::system.currency.edit')
            ->with(compact('currency', 'currencySymbolOptions', 'currencyCodeOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\System\Requests\CurrencyRequest $request
     * @param \Octa\Framework\Database\Models\Currency  $currency
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CurrencyRequest $request, Currency $currency)
    {
        $currency->update($request->all());

        return redirect()->route('admin.currency.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa::system.currency.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\Currency  $currency
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();

        return response()->json([
            'success' => true,
            'message' => __('octa::system.notification.delete', ['attribute' => __('octa::system.currency.title')]),
        ]);
    }
}
