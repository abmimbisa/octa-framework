<?php

namespace Octa\Framework\System\Controllers;

use Octa\Framework\Support\Facades\Widget;

class DashboardController
{
    /**
     * Show Dashboard of an Octa Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $orderWidget = Widget::get('octa-total-order');
        $customerWidget = Widget::get('octa-total-customer');
        $revenueWidget = Widget::get('octa-total-revenue');

        return view('octa::admin', compact('orderWidget', 'customerWidget', 'revenueWidget'));
    }
}
