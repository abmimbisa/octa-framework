<?php

namespace Octa\Framework\System\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\State;
use Octa\Framework\System\Requests\StateRequest;
use Octa\Framework\Database\Contracts\StateModelInterface;
use Octa\Framework\Database\Contracts\CountryModelInterface;

class StateController extends Controller
{
    /**
     * State Repository for the State Controller.
     * @var \Octa\Framework\Database\Repository\StateRepository
     */
    protected $stateRepository;

    /**
     * Country Repository for the State Controller.
     * @var \Octa\Framework\Database\Repository\CountryRepository
     */
    protected $countryRepository;

    /**
     * Construct for the Octa state controller.
     * @param \Octa\Framework\Database\Contracts\StateModelInterface $stateRepository
     * @param \Octa\Framework\Database\Contracts\CountryModelInterface $countryRepository
     */
    public function __construct(
        StateModelInterface $stateRepository,
        CountryModelInterface $countryRepository
    ) {
        $this->stateRepository = $stateRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $states = $this->stateRepository->all();

        return view('octa::system.state.index')
            ->with(compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('system.state');
        $countryOptions = $this->countryRepository->options();

        return view('octa::system.state.create')
            ->with(compact('countryOptions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\System\Requests\StateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StateRequest $request)
    {
        $this->stateRepository->create($request->all());

        return redirect()->route('admin.state.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::system.state.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\State $state
     * @return \Illuminate\View\View
     */
    public function edit(State $state)
    {
        $tabs = Tab::get('system.state');
        $countryOptions = $this->countryRepository->options();

        return view('octa::system.state.edit')
            ->with(compact('state', 'countryOptions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\System\Requests\StateRequest $request
     * @param \Octa\Framework\Database\Models\State  $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StateRequest $request, State $state)
    {
        $state->update($request->all());

        return redirect()->route('admin.state.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa:system.state.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\State  $state
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(State $state)
    {
        $state->delete();

        return response()->json([
            'success' => true,
            'message' => __('octa::system.notification.delete', ['attribute' => __('octa:system.state.title')]),
        ]);
    }
}
