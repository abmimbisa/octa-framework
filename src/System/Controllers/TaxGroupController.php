<?php

namespace Octa\Framework\System\Controllers;

use Octa\Framework\Database\Models\TaxGroup;
use Octa\Framework\System\Requests\TaxGroupRequest;
use Octa\Framework\Database\Contracts\TaxGroupModelInterface;

class TaxGroupController
{
    /**
     * TaxGroup Repository for Controller.
     * @var \Octa\Framework\Database\Repository\TaxGroupRepository
     */
    protected $taxGroupRepository;

    /**
     * Construct for the Octa tax group controller.
     * @param \Octa\Framework\Database\Contracts\TaxGroupModelInterface $taxGroupRepository
     */
    public function __construct(
        TaxGroupModelInterface $taxGroupRepository
    ) {
        $this->taxGroupRepository = $taxGroupRepository;
    }

    /**
     * Show Dashboard of an Octa Admin.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taxGroups = $this->taxGroupRepository->all();

        return view('octa::system.tax-group.index')
            ->with(compact('taxGroups'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('octa::system.tax-group.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\Cms\Requests\TaxGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaxGroupRequest $request)
    {
        $this->taxGroupRepository->create($request->all());

        return redirect()->route('admin.tax-group.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::system.tax-group.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\TaxGroup $taxGroup
     * @return \Illuminate\View\View
     */
    public function edit(TaxGroup $taxGroup)
    {
        return view('octa::system.tax-group.edit')
            ->with(compact('taxGroup'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\Cms\Requests\TaxGroupRequest $request
     * @param \Octa\Framework\Database\Models\TaxGroup  $taxGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaxGroupRequest $request, TaxGroup $taxGroup)
    {
        $taxGroup->update($request->all());

        return redirect()->route('admin.tax-group.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa::system.tax-group.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\TaxGroup  $taxGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TaxGroup $taxGroup)
    {
        $taxGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'octa::system.notification.delete',
                ['attribute' => __('octa::system.tax-group.title')]
            ),
        ]);
    }
}
