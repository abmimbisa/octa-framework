<?php

namespace Octa\Framework\Support\Console;

use Illuminate\Console\Command;
use Octa\Framework\Database\Contracts\AdminUserModelInterface;
use Octa\Framework\Database\Contracts\RoleModelInterface;

class AdminMakeCommand extends Command
{
    protected $roleRepository;

    protected $adminUserRepository;

    public function __construct(RoleModelInterface $roleRepository, AdminUserModelInterface $adminUserRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->adminUserRepository = $adminUserRepository;

        parent::__construct();
    }

    protected $name = 'octa:admin:make';

    protected $description = 'Create an Admin User for the Octa E-commerce';

    public function handle()
    {
        $data['first_name'] = $this->ask('Enter your first name?');
        $data['last_name'] = $this->ask('Enter your last name?');
        $data['email'] = $this->ask('Enter your email address?');
        $data['password'] = $this->secret('Enter your password');
        $data['confirm_password'] = $this->secret('Confirm your password');

        $role = $this->roleRepository->findAdminRole();
        $data['role_id'] = $role->id;
        $data['is_super_admin'] = 1;
        $data['password'] = bcrypt($data['password']);
        $this->adminUserRepository->create($data);

        $this->info('Admin User created successfully!');
    }
}
