<?php

namespace Octa\Framework\Support\Middlewares;

use Closure;
use Illuminate\Support\Facades\Session;
use Octa\Framework\Database\Contracts\CurrencyModelInterface;

class OctaCore
{
    protected $currencyRepository;

    public function __construct(CurrencyModelInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function handle($request, Closure $next)
    {
        $this->setDefaultCurrency();

        return $next($request);
    }

    public function setDefaultCurrency()
    {
        if (!Session::has('default_currency')) {
            $currency = $this->currencyRepository->all()->first();
            Session::put('default_currency', $currency);
        }
    }
}
