<?php

namespace Octa\Framework\Support\Providers;

use Octa\Framework\Tab\Manager;
use Octa\Framework\Tab\TabItem;
use Illuminate\Support\ServiceProvider;
use Octa\Framework\Support\Facades\Tab;

class TabProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = true;

    public function boot()
    {
        $this->registerTabs();
    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->registerManager();
        $this->app->singleton('tab', 'Octa\Framework\Tab\Manager');
    }

    /**
     * Register the tab Manager Instance.
     * @return void
     */
    protected function registerManager()
    {
        $this->app->singleton('tab', function () {
            new Manager();
        });
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return ['tab', 'Octa\Framework\Tab\Manager'];
    }

    /**
     * Register Tabs for the Different CRUD operations.
     * @return void
     */
    public function registerTabs()
    {

        Tab::put('promotion.promotion-code', function (TabItem $tab) {
            $tab->key('promotion.promotion.code')
                ->label('octa::system.tab.basic_info')
                ->view('octa::promotion.promotion-code._fields');
        });


        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::catalog.product._fields');
        });

        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.image')
                ->label('octa::system.tab.images')
                ->view('octa::catalog.product.cards.images');
        });
        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.property')
                ->label('octa::system.tab.property')
                ->view('octa::catalog.product.cards.property');
        });
        Tab::put('catalog.product', function (TabItem $tab) {
            $tab->key('catalog.product.attribute')
                ->label('octa::system.tab.attribute')
                ->view('octa::catalog.product.cards.attribute');
        });

        /****** CATALOG CATEGORY TABS *******/
        Tab::put('catalog.category', function (TabItem $tab) {
            $tab->key('catalog.category.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::catalog.category._fields');
        });

        /****** CATALOG PROPERTY TABS *******/
        Tab::put('catalog.property', function (TabItem $tab) {
            $tab->key('catalog.property.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::catalog.property._fields');
        });
        /****** CATALOG ATTRIBUTE TABS *******/
        Tab::put('catalog.attribute', function (TabItem $tab) {
            $tab->key('catalog.attribute.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::catalog.attribute._fields');
        });
        /******CMS PAGES TABS  *******/
        Tab::put('cms.page', function (TabItem $tab) {
            $tab->key('cms.page.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::cms.page._fields');
        });
        /******ORDER ORDER STATUS TABS  *******/
        Tab::put('order.order-status', function (TabItem $tab) {
            $tab->key('order.order-status.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::order.order-status._fields');
        });
        /******USER USER GROUP TABS  *******/
        Tab::put('user.user-group', function (TabItem $tab) {
            $tab->key('user.user-group.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::user.user-group._fields');
        });
        /******USER ADMIN USER TABS  *******/
        Tab::put('user.admin-user', function (TabItem $tab) {
            $tab->key('user.admin-user.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::user.admin-user._fields');
        });
        /******SYSTEM CURRENCY TABS  *******/
        Tab::put('system.currency', function (TabItem $tab) {
            $tab->key('system.currency.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::system.currency._fields');
        });
        /******SYSTEM STATE TABS  *******/
        Tab::put('system.state', function (TabItem $tab) {
            $tab->key('system.state.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::system.state._fields');
        });
        /******SYSTEM ROLE TABS  *******/
        Tab::put('system.role', function (TabItem $tab) {
            $tab->key('system.role.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::system.role._fields');
        });
        /******SYSTEM ROLE TABS  *******/
        Tab::put('system.language', function (TabItem $tab) {
            $tab->key('system.language.info')
                ->label('octa::system.tab.basic_info')
                ->view('octa::system.language._fields');
        });

        /******SYSTEM CONFIGURATION TABS  *******/
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.basic')
                ->label('octa::system.tab.basic_configuration')
                ->view('octa::system.configuration.cards.basic');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.user')
                ->label('octa::system.tab.user_configuration')
                ->view('octa::system.configuration.cards.user');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.tax')
                ->label('octa::system.tab.tax_configuration')
                ->view('octa::system.configuration.cards.tax');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.shipping')
                ->label('octa::system.tab.shipping_configuration')
                ->view('octa::system.configuration.cards.shipping');
        });
        Tab::put('system.configuration', function (TabItem $tab) {
            $tab->key('system.configuration.payment')
                ->label('octa::system.tab.payment_configuration')
                ->view('octa::system.configuration.cards.payment');
        });
    }
}
