<?php

namespace Octa\Framework\Support\Providers;

use Illuminate\Support\ServiceProvider;
use Octa\Framework\Database\Repository\MenuRepository;
use Octa\Framework\Database\Repository\PageRepository;
use Octa\Framework\Database\Repository\RoleRepository;
use Octa\Framework\Database\Repository\OrderRepository;
use Octa\Framework\Database\Repository\StateRepository;
use Octa\Framework\Database\Contracts\MenuModelInterface;
use Octa\Framework\Database\Contracts\PageModelInterface;
use Octa\Framework\Database\Contracts\RoleModelInterface;
use Octa\Framework\Database\Repository\AddressRepository;
use Octa\Framework\Database\Repository\CountryRepository;
use Octa\Framework\Database\Repository\ProductRepository;
use Octa\Framework\Database\Repository\TaxRateRepository;
use Octa\Framework\Database\Contracts\OrderModelInterface;
use Octa\Framework\Database\Contracts\StateModelInterface;
use Octa\Framework\Database\Repository\CategoryRepository;
use Octa\Framework\Database\Repository\CurrencyRepository;
use Octa\Framework\Database\Repository\LanguageRepository;
use Octa\Framework\Database\Repository\PropertyRepository;
use Octa\Framework\Database\Repository\TaxGroupRepository;
use Octa\Framework\Database\Repository\AdminUserRepository;
use Octa\Framework\Database\Repository\AttributeRepository;
use Octa\Framework\Database\Repository\MenuGroupRepository;
use Octa\Framework\Database\Repository\UserGroupRepository;
use Octa\Framework\Database\Contracts\AddressModelInterface;
use Octa\Framework\Database\Contracts\CountryModelInterface;
use Octa\Framework\Database\Contracts\ProductModelInterface;
use Octa\Framework\Database\Contracts\TaxRateModelInterface;
use Octa\Framework\Database\Repository\PermissionRepository;
use Octa\Framework\Database\Contracts\CategoryModelInterface;
use Octa\Framework\Database\Contracts\CurrencyModelInterface;
use Octa\Framework\Database\Contracts\LanguageModelInterface;
use Octa\Framework\Database\Contracts\PropertyModelInterface;
use Octa\Framework\Database\Contracts\TaxGroupModelInterface;
use Octa\Framework\Database\Repository\OrderStatusRepository;
use Octa\Framework\Database\Contracts\AdminUserModelInterface;
use Octa\Framework\Database\Contracts\AttributeModelInterface;
use Octa\Framework\Database\Contracts\MenuGroupModelInterface;
use Octa\Framework\Database\Contracts\UserGroupModelInterface;
use Octa\Framework\Database\Repository\OrderProductRepository;
use Octa\Framework\Database\Repository\ProductImageRepository;
use Octa\Framework\Database\Repository\PromotionCodeRepository;
use Octa\Framework\Database\Contracts\PermissionModelInterface;
use Octa\Framework\Database\Repository\ConfigurationRepository;
use Octa\Framework\Database\Contracts\OrderStatusModelInterface;
use Octa\Framework\Database\Repository\CategoryFilterRepository;
use Octa\Framework\Database\Contracts\OrderProductModelInterface;
use Octa\Framework\Database\Contracts\ProductImageModelInterface;
use Octa\Framework\Database\Contracts\PromotionCodeModelInterface;
use Octa\Framework\Database\Contracts\ConfigurationModelInterface;
use Octa\Framework\Database\Contracts\CategoryFilterModelInterface;
use Octa\Framework\Database\Repository\AttributeProductValueRepository;
use Octa\Framework\Database\Repository\OrderProductAttributeRepository;
use Octa\Framework\Database\Repository\AttributeDropdownOptionRepository;
use Octa\Framework\Database\Contracts\AttributeProductValueModelInterface;
use Octa\Framework\Database\Contracts\OrderProductAttributeModelInterface;
use Octa\Framework\Database\Contracts\AttributeDropdownOptionModelInterface;

class ModelProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Models Array list to bind with It's Contact.
     * @var array
     */
    protected $models = [
        AddressModelInterface::class => AddressRepository::class,
        AdminUserModelInterface::class => AdminUserRepository::class,
        AttributeModelInterface::class => AttributeRepository::class,
        AttributeDropdownOptionModelInterface::class => AttributeDropdownOptionRepository::class,
        AttributeProductValueModelInterface::class => AttributeProductValueRepository::class,
        CategoryModelInterface::class => CategoryRepository::class,
        CategoryFilterModelInterface::class => CategoryFilterRepository::class,
        ConfigurationModelInterface::class => ConfigurationRepository::class,
        CountryModelInterface::class => CountryRepository::class,
        CurrencyModelInterface::class => CurrencyRepository::class,
        LanguageModelInterface::class => LanguageRepository::class,
        OrderModelInterface::class => OrderRepository::class,
        OrderProductModelInterface::class => OrderProductRepository::class,
        OrderProductAttributeModelInterface::class => OrderProductAttributeRepository::class,
        OrderStatusModelInterface::class => OrderStatusRepository::class,
        PermissionModelInterface::class => PermissionRepository::class,
        PageModelInterface::class => PageRepository::class,
        PromotionCodeModelInterface::class => PromotionCodeRepository::class,
        ProductModelInterface::class => ProductRepository::class,
        ProductImageModelInterface::class => ProductImageRepository::class,
        MenuModelInterface::class => MenuRepository::class,
        MenuGroupModelInterface::class => MenuGroupRepository::class,
        PropertyModelInterface::class => PropertyRepository::class,
        RoleModelInterface::class => RoleRepository::class,
        StateModelInterface::class => StateRepository::class,
        UserGroupModelInterface::class => UserGroupRepository::class,
        TaxGroupModelInterface::class => TaxGroupRepository::class,
        TaxRateModelInterface::class => TaxRateRepository::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerModelContracts();
    }

    /**
     * Bind The Eloquent Model with their contract.
     *
     * @return void
     */
    protected function registerModelContracts()
    {
        foreach ($this->models as $interface => $repository) {
            $this->app->bind($interface, $repository);
        }
    }
}
