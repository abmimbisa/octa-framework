<?php

namespace Octa\Framework\Support\Facades;

use Illuminate\Support\Facades\Facade;

class Tab extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tab';
    }
}
