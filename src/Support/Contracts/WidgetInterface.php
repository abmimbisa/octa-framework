<?php

namespace Octa\Framework\Support\Contracts;

interface WidgetInterface
{
    public function label();

    public function type();
}
