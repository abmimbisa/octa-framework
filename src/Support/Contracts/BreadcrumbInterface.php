<?php

namespace Octa\Framework\Support\Contracts;

interface BreadcrumbInterface
{
    public function label();

    public function route();
}
