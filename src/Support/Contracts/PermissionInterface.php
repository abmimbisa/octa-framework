<?php

namespace Octa\Framework\Support\Contracts;

interface PermissionInterface
{
    public function key($key = null);

    public function label($label = null);

    public function routes($icon = null);
}
