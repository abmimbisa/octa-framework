<?php

namespace Octa\Framework\Catalog\Controllers;

use Octa\Framework\Support\Facades\Tab;
use Octa\Framework\Database\Models\Category;
use Octa\Framework\Catalog\Requests\CategoryRequest;
use Octa\Framework\Database\Contracts\CategoryModelInterface;

class CategoryController
{
    /**
     * Category Repository for the Install Command.
     * @var \Octa\Framework\Database\Repository\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Construct for the Octa install command.
     * @param \Octa\Framework\Database\Contracts\CategoryModelInterface $categoryRepository
     */
    public function __construct(
        CategoryModelInterface $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Show Category Index Page.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = $this->categoryRepository->all();

        return view('octa::catalog.category.index')
            ->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tab::get('catalog.category');

        return view('octa::catalog.category.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Octa\Framework\Catalog\Requests\CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        $this->categoryRepository->create($request->all());

        return redirect()->route('admin.category.index')
            ->with('successNotification', __(
                'octa::system.notification.store',
                ['attribute' => __('octa::catalog.category.title')]
            ));
    }

    /**
     * Show the form for editing the specified resource.
     * @param \Octa\Framework\Database\Models\Category $category
     * @return \Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $tabs = Tab::get('catalog.category');

        return view('octa::catalog.category.edit')
            ->with(compact('category', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Octa\Framework\Catalog\Requests\CategoryRequest $request
     * @param \Octa\Framework\Database\Models\Category  $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('admin.category.index')
            ->with('successNotification', __(
                'octa::system.notification.updated',
                ['attribute' => __('octa::catalog.category.title')]
            ));
    }

    /**
     * Remove the specified resource from storage.
     * @param \Octa\Framework\Database\Models\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([
            'success' => true,
            'message' => __(
                'octa::system.notification.delete',
                ['attribute' => __('octa::catalog.category.title')]
            ),
        ]);
    }
}
