@extends('octa::layouts.app')

@section('meta_title')
    {{ __('octa::cms.menu.create.title') }}: Octa E commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('octa::cms.menu.create.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <menu-save
            :prop-categories="{{ $categories }}"
            :prop-front-menus="{{ $frontMenus }}"
            :menu-group="{{ $menuGroup }}"
            :prop-menus="{{ $menus }}"
            base-url="{{ asset(config('octa.admin_url')) }}"
            inline-template>
        <div>
            <a-row :gutter="30" class="menu-save-page">
                <a-form
                    :form="form"
                    v-on:submit="handleSubmit"
                    method="post"
                    action="{{ route('admin.menu-group.update', $menuGroup->id) }}">

                    @csrf
                    @method('put')
                    @include('octa::cms.menu._fields')
                    <a-col class="mt-1" :span="24">
                        <a-form-item>
                            <a-button type="primary" html-type="submit">
                                {{ __('octa::system.btn.save') }}
                            </a-button>

                            <a-button class="ml-1" type="default" v-on:click.prevent="cancelMenu">
                                {{ __('octa::system.btn.cancel') }}
                            </a-button>
                        </a-form-item>
                    </a-col>
                </a-form>
            </a-row>
        </div>
        </menu-save>
    </a-col>
</a-row>
@endsection
