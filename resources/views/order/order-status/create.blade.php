@extends('octa::layouts.app')

@section('meta_title')
    {{ __('octa::order.order-status.create.title') }}: Octa E commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('octa::order.order-status.create.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <order-status-save base-url="{{ asset(config('octa.admin_url')) }}" inline-template>
        <div>
            <a-form
                :form="orderStatusForm"
                method="post"
                action="{{ route('admin.order-status.store') }}"
                @submit="handleSubmit"
            >
                @csrf
                <a-tabs tabbar-gutter="15" tab-position="left" default-active-key="order.order-status.info">
                @foreach ($tabs as $tab)
                    <a-tab-pane :force-render="true" tab="{{ $tab->label() }}" key="{{ $tab->key() }}">
                        @php
                            $path = $tab->view();
                        @endphp
                        @include($path)
                    </a-tab-pane>
                @endforeach
                </a-tabs>

                <a-form-item>
                    <a-button
                        type="primary"
                        html-type="submit"
                    >
                        {{ __('octa::system.btn.save') }}
                    </a-button>

                    <a-button
                        class="ml-1"
                        type="default"
                        v-on:click.prevent="cancelOrderStatus"
                    >
                        {{ __('octa::system.btn.cancel') }}
                    </a-button>
                </a-form-item>
            </a-form>
            </div>
        </order-status-save>
    </a-col>
</a-row>
@endsection
