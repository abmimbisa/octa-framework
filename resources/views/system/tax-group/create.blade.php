@extends('octa::layouts.app')

@section('meta_title')
    {{ __('octa::system.tax-group.create.title') }}: Octa E commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('octa::system.tax-group.create.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <tax-group-save base-url="{{ asset(config('octa.admin_url')) }}" inline-template>
        <div>
            <a-form
                :form="taxGroupForm"
                method="post"
                action="{{ route('admin.tax-group.store') }}"
                @submit="handleSubmit"
            >
                @csrf
                @include('octa::system.tax-group._fields')

                <a-form-item>
                    <a-button
                        type="primary"
                        html-type="submit"
                    >
                        {{ __('octa::system.btn.save') }}
                    </a-button>

                    <a-button
                        class="ml-1"
                        type="default"
                        v-on:click.prevent="cancelTaxGroup"
                    >
                        {{ __('octa::system.btn.cancel') }}
                    </a-button>
                </a-form-item>
            </a-form>
            </div>
        </tax-group-save>
    </a-col>
</a-row>
@endsection
