@extends('octa::layouts.app')

@section('meta_title')
    {{ __('octa::system.tax-rate.create.title') }}: Octa E commerce Admin Dashboard
@endsection

@section('page_title')
    {{ __('octa::system.tax-rate.create.title') }}
@endsection

@section('content')
<a-row type="flex" justify="center">
    <a-col :span="24">
        <tax-rate-save base-url="{{ asset(config('octa.admin_url')) }}" inline-template>
        <div>
            <a-form
                :form="taxRateForm"
                method="post"
                action="{{ route('admin.tax-rate.store') }}"
                @submit="handleSubmit"
            >
                @csrf
                @include('octa::system.tax-rate._fields')

                <a-form-item>
                    <a-button
                        type="primary"
                        html-type="submit"
                    >
                        {{ __('octa::system.btn.save') }}
                    </a-button>

                    <a-button
                        class="ml-1"
                        type="default"
                        v-on:click.prevent="cancelTaxRate"
                    >
                        {{ __('octa::system.btn.cancel') }}
                    </a-button>
                </a-form-item>
            </a-form>
            </div>
        </tax-rate-save>
    </a-col>
</a-row>
@endsection
