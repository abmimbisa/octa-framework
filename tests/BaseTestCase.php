<?php

namespace Octa\Framework\Tests;

use Octa\Framework\OctaProvider;
use Octa\Framework\Database\Contracts\CurrencyModelInterface;
use Faker\Generator as FakerGenerator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;
use Octa\Framework\Database\Models\AdminUser;
use Octa\Framework\Database\Models\Currency;
use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

abstract class BaseTestCase extends OrchestraTestCase
{
    /**
     * Admin User.
     * @var \Octa\Framework\Database\Models\AdminUser
     */
    protected $user;

    protected $faker;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->faker = $this->app->make(FakerGenerator::class);

        $this->withFactories(__DIR__ . '/../database/factories');

        $this->setUpDatabase();
        $this->setDefaultCurrency();
        Notification::fake();
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application   $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'base64:UTyp33UhGolgzCK5CJmT+hNHcA+dJyp3+oINtX+VoPI=');
    }

    /**
     * Reset the Database.
     * @return void
     */
    private function resetDatabase(): void
    {
        $this->artisan('migrate');
    }

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return array
     */
    protected function getPackageProviders($app): array
    {
        return [
            OctaProvider::class,
        ];
    }

    /**
     * Setup database for the unit test.
     * @return void
     */
    protected function setUpDatabase(): void
    {
        $this->loadLaravelMigrations();
        $this->resetDatabase();
    }

    protected function setDefaultCurrency()
    {
        factory(Currency::class)->create();
        $currencyRepository = app(CurrencyModelInterface::class);
        $currency = $currencyRepository->all()->first();
        $this->withSession(['default_currency' => $currency]);
    }

    /**
     * Get package aliases.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return array
     */
    protected function getPackageAliases($app): array
    {
        return [
            //'AdminMenu' => 'Octa\\Framework\\AdminMenu\\Facade',
            //'AdminConfiguration' => 'Octa\\Framework\\AdminConfiguration\\Facade',
            //'DataGrid' => 'Octa\\Framework\\DataGrid\\Facade',
            //'Image' => 'Octa\\Framework\\Image\\Facade',
            'Breadcrumb' => \Octa\Framework\Support\Facades\Breadcrumb::class,
            'Menu' => \Octa\Framework\Support\Facades\Menu::class,
            'Module' => \Octa\Framework\Support\Facades\Module::class,
            'Permission' => \Octa\Framework\Support\Facades\Permission::class,
            'Cart' => Octa\Framework\Support\Facades\Cart::class,
            'Payment' => Octa\Framework\Support\Facades\Payment::class,
            'Shipping' => Octa\Framework\Support\Facades\Shipping::class,
            'Tab' => Octa\Framework\Support\Facades\Tab::class,
            //'Theme' => 'Octa\\Framework\\Theme\\Facade',
            'Widget' => Octa\Framework\Support\Facades\Widget::class
        ];
    }

    /**
     * Create an Admin user model.
     * @param array $data
     * @return self
     */
    protected function createAdminUser($data = ['is_super_admin' => 1]): self
    {
        if (null === $this->user) {
            $this->user = factory(AdminUser::class)->create($data);
        }

        return $this;
    }
}
