<?php

namespace Octa\Framework\Tests\Functional;

use Octa\Framework\Tests\BaseTestCase;
use Octa\Framework\System\Controllers\LoginController;

/** @runInSeparateProcess */
class UserTest extends BaseTestCase
{
    public function testRedirectPathForLoginController()
    {
        $loginController = app(LoginController::class);
        $adminPath = config('octa.admin_url');
        $this->assertEquals($loginController->redirectPath(), $adminPath);
    }
}
